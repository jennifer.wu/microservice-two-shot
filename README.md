# Wardrobify

Team:

* Person 1 - Which microservice?    Jennifer Wu - Hats
* Person 2 - Which microservice?    Teri Wu - Shoes

## Design

## Shoes microservice

A Shoe model will be created with the fields: manufacturer, model_name,color, and picture_url. A Bin value object model will also be created in addition to a poll.py that includes a get request to read Bin ojbects. Afterwards, api views will be created and plugged into Insomnia. Finally working on React after the backend is built.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.


Model will include: fabric, style name, color, URL for 
picture, and location in the wardrobe (closet name, 
section number, and shelf number).  Create a VOmodel for
location.  Poll data from Wardrobe API for location data
to use.


